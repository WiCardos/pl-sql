--Based on
--https://dba.stackexchange.com/questions/11047/how-to-retrieve-foreign-key-constraints-data?newreg=ecac62069dbd4d979b8eb61c2f6c9438

select
   cons.constraint_name   "Constraint"
   ,cons.table_name        "Constraint's table"
   ,cols.column_name       "Constraint's column"
   --points_to
   ,cons_pt.constraint_name "Pointed constraint"
   ,cons_pt.table_name      "Pointed constraint's table"
   ,cols_pt.column_name     "Pointed constraint's column"
   --referenced_by
   ,cons_rb.constraint_name "Referenced constraint"
   ,cons_rb.table_name      "Referenced constraint's table"
   ,cols_rb.column_name     "Referenced constraint's column"
from
   user_constraints  cons
   left join user_cons_columns cols on cols.constraint_name = cons.constraint_name
   --points_to
   left join user_constraints  cons_pt on cons_pt.constraint_name = cons.r_constraint_name
   left join user_cons_columns cols_pt on cols_pt.constraint_name = cons.r_constraint_name
   --referenced_by
   left join user_constraints  cons_rb on cons.constraint_name in cons_rb.r_constraint_name
   left join user_cons_columns cols_rb on cols_rb.constraint_name = cons_rb.constraint_name
;
