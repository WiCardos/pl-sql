delete my_table where rowid not in (
   select
      min(rowid)    --min rowid is the first version of that row to have been inserted.
   from 
      my_table
   group by --every column that garanties the uniqueness of the rows must be in the group by, otherwise more rows will be deleted.
      column_1
      ,coumn_2
      --...
      ,column_n
);
