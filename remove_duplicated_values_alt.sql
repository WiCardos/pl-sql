delete my_table a
where rowid > (
   select
      min(rowid) --min rowid is the first version of that row to have been inserted.
   from
      my_table b
   where
      a.column_1 = b.column_1 --every column that garanties the uniqueness of the rows must be in the where clause, otherwise more rows will be deleted.
   and a.column_2 = b.column_2
   --...
   and a.column_n = b.column_n
);
