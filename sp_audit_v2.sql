--based on https://blogs.oracle.com/connect/post/error-management

--drop sequence seq_audit;
--drop table tab_audit;

create sequence  seq_audit  minvalue 1 maxvalue 999999999999999999999999999 increment by 1 start with 1 nocache  noorder  nocycle  nokeep  noscale  global ;

create table tab_audit(
    id                  number          default seq_audit.nextval
    ,error_stack        varchar2(4000)
    ,error_backtrace    clob
    ,error_callstack    clob
    ,created_on         date            default sysdate
    ,created_by         varchar2(30)    default user
);

--add to the end of each procedure.
--    exception     
--        when others then
--           sp_audit(
--            i_stack       =>  sys.dbms_utility.format_error_stack
--            ,i_backtrace  =>  sys.dbms_utility.format_error_backtrace
--            ,i_callstack  =>  sys.dbms_utility.format_call_stack
--           );

create or replace procedure sp_audit(
    i_stack         in varchar2
    ,i_backtrace    in clob
    ,i_callstack    in clob
)as

    pragma autonomous_transaction;
begin
    insert into tab_audit(
        error_stack
        ,error_backtrace
        ,error_callstack
    )values(
        i_stack
        ,i_backtrace
        ,i_callstack
    );
    commit;
end sp_audit;
