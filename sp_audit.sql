--DROP SEQUENCE SEQ_AUDIT;
--DROP TABLE TAB_AUDIT;

CREATE SEQUENCE  SEQ_AUDIT  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;

CREATE TABLE TAB_AUDIT(
    ID                  number  default SEQ_AUDIT.nextval
    ,ERROR_STACK        varchar2(200 char)
    ,ERROR_BACKTRACE    varchar2(200 char)
    ,DATE               date    default sysdate
);

--Add to the end of each procedure.
--    EXCEPTION     
--        WHEN OTHERS THEN
--           SP_AUDIT(
--            i_stack       =>  substr(DBMS_UTILITY.FORMAT_ERROR_STACK ,1, 200)
--            ,i_backtrace  =>  substr(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE ,1, 200)
--           );

create or replace procedure SP_AUDIT(
    i_stack         in varchar2
    ,i_backtrace    in varchar2
)as

    pragma autonomous_transaction;
begin
    insert into TAB_AUDIT(
        error_stack
        ,error_backtrace
    )values(
        i_stack
        ,i_backtrace
    );
    commit;
end SP_AUDIT;
