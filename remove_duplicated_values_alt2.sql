delete from my_table where rowid in (
   select rid from (
      select
         rowid rid
         ,row_number() over (partition by
            column_1 --every column that garanties the uniqueness of the rows must be in the row_number analytic function
            ,column_2
            --, ...
            , column_n 
         order by rownum) rnum
      from my_table
   )
   where rnum > 1
);
